import java.io.File
import java.util.Collections
import kotlin.math.absoluteValue

data class Position(val x: Int, val y: Int) {
    operator fun plus(other: Position): Position {
        return Position(this.x + other.x, this.y + other.y)
    }
}

fun Position.canMove(elfs: HashSet<Position>, positionOffsets: List<Position>): Boolean {
    positionOffsets.forEach { positionOffset ->
        if ((this + positionOffset) in elfs) {
            return false
        }
    }
    return true
}


fun Position.getProposedPosition(elfs: HashSet<Position>, directions: List<List<Position>>): Position {
    val possibleDirections = arrayOf(this, this, this, this)
    directions.forEachIndexed { i, direction ->
        if(this.canMove(elfs, direction)) {
            possibleDirections[i] = this + direction[1]
        }
    }
    if (possibleDirections.filter { it != this }.size in arrayOf(0, 4)) {
        return this
    }
    return possibleDirections.first { it != this }
}

fun main() {
    val startTime = System.nanoTime()

    val input = File("input.txt")
        .readLines()
        .map { line -> line.map { it == '#' } }

    val directions = listOf(
        listOf(Position(-1, -1), Position( 0, -1), Position(+1, -1)), // North
        listOf(Position(-1, +1), Position( 0, +1), Position(+1, +1)), // South
        listOf(Position(-1, -1), Position(-1,  0), Position(-1, +1)), // West
        listOf(Position(+1, -1), Position(+1,  0), Position(+1, +1))  // East
    )

    val proposedPositions = mutableMapOf<Position, Position>()
    input.forEachIndexed { y, line ->
        line.forEachIndexed { x, isElf ->
            if (isElf) {
                proposedPositions[Position(x, y)] = Position(x, y)
            }
        }
    }

    var elfs: HashSet<Position>
    var nextElfs = proposedPositions.values.toHashSet()

    var round = 0
    while(true) {
        // move
        elfs = nextElfs
        // get proposed positions
        proposedPositions.clear()
        elfs.forEach { position ->
            proposedPositions[position] = position.getProposedPosition(elfs, directions)
        }

        // remove duplicate proposed positions
        val duplicates = proposedPositions.values
            .groupingBy { it }
            .eachCount()
            .filter { (_, count) -> count >= 2 }
            .keys
        duplicates.forEach { position ->
            proposedPositions.filter { it.value == position }.forEach { (current, _) ->
                proposedPositions[current] = current
            }
        }

        // shift starting direction
        Collections.rotate(directions, -1)

        if (round == 10) {
            val height = (elfs.maxOf { it.y } - elfs.minOf { it.y }).absoluteValue + 1
            val width = (elfs.maxOf { it.x } - elfs.minOf { it.x }).absoluteValue + 1
            println("part 1 answer: ${ height * width - elfs.size }")
        }
        nextElfs = proposedPositions.values.toHashSet()
        if (elfs == nextElfs) {
            break
        }

        round++
    }
    val endTime = System.nanoTime()

    println("part 2 answer: ${ round + 1 }")
    println("done in ${ (endTime - startTime) / 1_000_000 }ms")
}