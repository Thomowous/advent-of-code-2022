import java.io.File

const val width = 40
const val height = 6

fun part1 (cycle: Int, register: Int): Int {
    if ((cycle + 20) % width == 0) {
        return cycle * register
    }
    return 0
}

fun part2 (cycle: Int, register: Int, crt: Array<Array<Char>>): Array<Array<Char>> {
    if (cycle % width in (register - 1 .. register + 1)) {
        crt[cycle / width][(cycle % width)] = '#'
    }
    return crt
}

fun main() {
    val input = File("input.txt")
        .readLines()

    var cycle = 0
    var register = 1
    var signal = 0
    var crt = Array(height) { Array(width) { ' ' } }
    input.forEach {line ->
        crt = part2(cycle, register, crt)
        cycle++
        signal += part1(cycle, register)

        // finish addx
        if (line != "noop") {
            crt = part2(cycle, register, crt)
            cycle++
            signal += part1(cycle, register)

            register += line.split(' ')[1].toInt()
        }
    }
    println("part 1 answer: $signal")
    println("part 2 answer:")
    crt.forEach { println(it.joinToString("")) }
}