import java.io.File

fun getResult(name: String, monkeys: Map<String, List<String>>, human: ULong): ULong {
    val job = monkeys[name]!!
    if (name == "humn") {
        return human
    }
    if (job.size == 1) {
        return job[0].toULong()
    }

    return when (job[1]) {
        "+" -> getResult(job[0], monkeys, human) + getResult(job[2], monkeys, human)
        "-" -> getResult(job[0], monkeys, human) - getResult(job[2], monkeys, human)
        "*" -> getResult(job[0], monkeys, human) * getResult(job[2], monkeys, human)
        "/" -> getResult(job[0], monkeys, human) / getResult(job[2], monkeys, human)
        else -> 0u
    }
}

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { it.split(": ") }
        .associate { (name, job) ->
            name to job.split(' ')
        }
    val originalHumanValue = input["humn"]!![0].toULong()


    // find on which side is human
    val left = getResult(input["root"]!![0], input, originalHumanValue)
    val right = getResult(input["root"]!![2], input, originalHumanValue)
    val leftSideHasHuman = left != getResult(input["root"]!![0], input, input["humn"]!![0].toULong() * 2u)
    val humanRootIndex = if (leftSideHasHuman) 0 else 2
    val otherResult = if (leftSideHasHuman) right else left

    // get boundaries (won't work for all input)
    var lowestHuman: ULong = 1u
    var highestHuman: ULong = 1u
    var lowestResult: ULong = getResult(input["root"]!![humanRootIndex], input, lowestHuman)
    while (lowestResult > otherResult) {
        lowestHuman *= 2u
        lowestResult = getResult(input["root"]!![humanRootIndex], input, lowestHuman)
    }

    // find the result
    var human: ULong = 0u
    var tmpResult: ULong = 0u
    while(tmpResult != otherResult) {
        human = (lowestHuman + highestHuman) / 2u
        tmpResult = getResult(input["root"]!![humanRootIndex], input, human)
        if (tmpResult > otherResult) {
            highestHuman = human
        } else {
            lowestHuman = human
        }
    }

    // pinpoint the result
    while (tmpResult == otherResult) {
        human--
        tmpResult = getResult(input["root"]!![humanRootIndex], input, human)
    }
    human++

    println("part 1 answer: ${ getResult("root", input, originalHumanValue) }")
    println("part 2 answer: $human")

}