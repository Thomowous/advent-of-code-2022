import java.io.File

fun main() {
    /* INPUT */
    val input = File("input.txt")
        .readText()
        .split("\n\n")
        .map { it.split("\n") }
        .map { arrIt -> arrIt.map { it.toInt() } }

    /* PART 1 */
    val resultPart1 = input.maxOf { it.sum() }
    println("part 1 answer: $resultPart1")

    /* PART 2 */
    val resultPart2 = input
        .map { it.sum() }
        .sortedDescending()
        .take(3)
        .sum()
    println("part 2 answer: $resultPart2")
}