import java.io.File

class Node(char: Char) {
    var visited = false
    var elevation = char - 'a'
    val neighbours: MutableList<Node> = mutableListOf()
    var distance = 0
}
fun step(node: Node, currentDistance: Int, part: Int) {
    node.visited = true
    if (part == 2 && node.elevation == 0) {
        node.distance = 0
    } else {
        node.distance = currentDistance
    }
    node.neighbours.forEach { nextNode ->
        if (!nextNode.visited || (node.distance + 1) < nextNode.distance) {
            step(nextNode, node.distance + 1, part)
        }
    }
}

fun main() {
    val input = File("input.txt").readLines()

    val nodes = Array(input.size) { Array(input[0].length) { Node('-') } }
    var startNode = Node('-')
    var endNode = Node('-')

    // fill nodes array
    input.forEachIndexed { y, line ->
        line.forEachIndexed { x, char ->
            nodes[y][x] = Node(char)
            if (char == 'S') {
                nodes[y][x].elevation = 0
                startNode = nodes[y][x]
            } else if (char == 'E') {
                nodes[y][x].elevation = 'z' - 'a'
                endNode = nodes[y][x]
            }
        }
    }

    // generate neighbours
    nodes.forEachIndexed { y, nodesLine ->
        nodesLine.forEachIndexed { x, node ->
            if (x > 0 && node.elevation + 1 >= nodes[y][x - 1].elevation) {
                node.neighbours.add(nodes[y][x - 1])
            }
            if (x < nodesLine.lastIndex && node.elevation + 1 >= nodes[y][x + 1].elevation) {
                node.neighbours.add(nodes[y][x + 1])
            }
            if (y > 0 && node.elevation + 1 >= nodes[y - 1][x].elevation) {
                node.neighbours.add(nodes[y - 1][x])
            }
            if (y < nodes.lastIndex && node.elevation + 1 >= nodes[y + 1][x].elevation) {
                node.neighbours.add(nodes[y + 1][x])
            }
        }
    }

    // part 1
    step(startNode, 0, 1)
    println("part 1 answer: ${ endNode.distance }")

    // reset visited bool
    nodes.forEach { nodesLine -> nodesLine.forEach { node -> node.visited = false } }

    // part 2
    step(startNode, 0, 2)
    println("part 2 answer: ${ endNode.distance }")
}