import java.io.File

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { line -> line.split(',', '-').map { it.toInt() } }

    val result1 = input.count{ (a, b, c, d) -> (a >= c && b <= d) || (c >= a && d <= b) }
    println("part 1 answer: $result1")

    val result2 = input.count{ (a, b, c, d) -> a <= d && b >= c }
    println("part 2 answer: $result2")
}