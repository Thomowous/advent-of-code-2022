import java.io.File
import java.util.LinkedList

const val ORE_ROBOT_INDEX = 0
const val CLAY_ROBOT_INDEX = 1
const val OBSIDIAN_ROBOT_INDEX = 2
const val GEODE_ROBOT_INDEX = 3

const val STARTING_TIME_PART_1 = 24
const val STARTING_TIME_PART_2 = 32

data class Quad(val ore: Int, val clay: Int, val obsidian: Int, val geode: Int)

operator fun Quad.plus(other: Quad): Quad {
    return Quad(this.ore + other.ore, this.clay + other.clay, this.obsidian + other.obsidian, this.geode + other.geode)
}
operator fun Quad.minus(other: Quad): Quad {
    return Quad(this.ore - other.ore, this.clay - other.clay, this.obsidian - other.obsidian, this.geode - other.geode)
}

fun canBuy(stock: Quad, cost: Quad): Boolean {
    if (stock.ore < cost.ore) return false
    if (stock.clay < cost.clay) return false
    if (stock.obsidian < cost.obsidian) return false
    return true
}

data class State(var minutesRemaining: Int, var robots: Quad, var stock: Quad, val boughtRobotPreviously: Boolean)

fun addMoves(state: State, costs: List<Quad>, maxes: Quad, queue: LinkedList<State>) {
    // ore robot
    val canBuyOreRobot = canBuy(state.stock, costs[ORE_ROBOT_INDEX])
    val canBuyOreRobotPrevious = canBuy((state.stock - state.robots), costs[ORE_ROBOT_INDEX])
    if (canBuyOreRobot and !canBuyOreRobotPrevious and (state.robots.ore < maxes.ore)) {
        queue.add(State(state.minutesRemaining - 1, state.robots + Quad(1, 0, 0, 0), state.stock - costs[ORE_ROBOT_INDEX] + state.robots, true))
    }

    // clay robot
    val canBuyClayRobot = canBuy(state.stock, costs[CLAY_ROBOT_INDEX])
    val canBuyClayRobotPrevious = canBuy((state.stock - state.robots), costs[CLAY_ROBOT_INDEX])
    if (canBuyClayRobot and !canBuyClayRobotPrevious and (state.robots.clay < maxes.clay)) {
        queue.add(State(state.minutesRemaining - 1, state.robots + Quad(0, 1, 0, 0), state.stock - costs[CLAY_ROBOT_INDEX] + state.robots, true))
    }

    // obsidian robot
    val canBuyObsidianRobot = canBuy(state.stock, costs[OBSIDIAN_ROBOT_INDEX])
    val canBuyObsidianRobotPrevious = canBuy((state.stock - state.robots), costs[OBSIDIAN_ROBOT_INDEX])
    if (canBuyObsidianRobot and (state.robots.obsidian < maxes.obsidian) and (!canBuyObsidianRobotPrevious or state.boughtRobotPreviously)) {
        queue.add(State(state.minutesRemaining - 1, state.robots + Quad(0, 0, 1, 0), state.stock - costs[OBSIDIAN_ROBOT_INDEX] + state.robots, true))
    }

    // geode robot
    val canBuyGeodeRobot = canBuy(state.stock, costs[GEODE_ROBOT_INDEX])
    val canBuyGeodeRobotPrevious = canBuy((state.stock - state.robots), costs[GEODE_ROBOT_INDEX])
    if (canBuyGeodeRobot and (!canBuyGeodeRobotPrevious or state.boughtRobotPreviously)) {
        queue.add(State(state.minutesRemaining - 1, state.robots + Quad(0, 0, 0, 1), state.stock - costs[GEODE_ROBOT_INDEX] + state.robots, true))
    }

    // wait if there is a robot that cant be bought at this moment
    if (!canBuyOreRobot or !canBuyClayRobot or !canBuyObsidianRobot or !canBuyGeodeRobot) {
        queue.add(State(state.minutesRemaining - 1, state.robots, state.stock + state.robots, false))
    }
}

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { line -> line.split(".", ":", "and")
            .dropLast(1)
            .map { part -> part.filter { char -> char.isDigit() }.toInt() }
        }.associate { costs ->
            costs[0] to listOf(
                Quad(costs[1], 0, 0, 0),
                Quad(costs[2], 0, 0, 0),
                Quad(costs[3], costs[4], 0, 0),
                Quad(costs[5], 0, costs[6], 0)
            )
        }

    val blueprintScores = Array(input.size) { 0 }
    input.forEach { (blueprintId, costs) ->
        val queue = LinkedList<State>()
        queue.add(State(STARTING_TIME_PART_1, Quad(1, 0, 0, 0), Quad(0, 0, 0, 0), false))
        val visited = hashSetOf<State>()
        var bestGeodes = 0
        val maxes = Quad(costs.maxOf { it.ore }, costs.maxOf { it.clay }, costs.maxOf { it.obsidian }, 0)
        while (queue.size > 0) {
            val state = queue.pop()
            if (state in visited) continue
            visited.add(state)
            bestGeodes = bestGeodes.coerceAtLeast(state.stock.geode)
            if (state.minutesRemaining == 0) continue
            addMoves(state, costs, maxes, queue)
        }

        println("$blueprintId * $bestGeodes = ${ blueprintId * bestGeodes }")
        blueprintScores[blueprintId - 1] = blueprintId * bestGeodes
    }
    println("===========")
    var geodesMultiplied = 1
    input.entries.take(3).forEach { (_, costs) ->
        val queue = LinkedList<State>()
        queue.add(State(STARTING_TIME_PART_2, Quad(1, 0, 0, 0), Quad(0, 0, 0, 0), false))
        var bestGeodes = 0
        val visited = hashSetOf<State>()
        val maxes = Quad(costs.maxOf { it.ore }, costs.maxOf { it.clay }, costs.maxOf { it.obsidian }, 0)
        while (queue.size > 0) {
            val state = queue.pop()
            if (state in visited) continue
            visited.add(state)
            bestGeodes = bestGeodes.coerceAtLeast(state.stock.geode)
            if (state.minutesRemaining == 0) continue
            addMoves(state, costs, maxes, queue)
        }

        println("$geodesMultiplied * $bestGeodes = ${ geodesMultiplied * bestGeodes }")
        geodesMultiplied *= bestGeodes
    }
    println("===========")
    println("part 1 answer: ${ blueprintScores.sum() }")
    println("part 2 answer: $geodesMultiplied")
}