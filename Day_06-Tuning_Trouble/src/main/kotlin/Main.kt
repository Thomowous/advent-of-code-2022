import java.io.File

fun main() {
    val input = File("input.txt")
        .readText()

    val windowSize1 = 4
    val result1 = input
        .windowed(windowSize1,1)
        .takeWhile { it.toSet().count() != windowSize1 }
        .count() + windowSize1
    println("part 1 answer: $result1")

    val windowSize2 = 14
    val result2 = input
        .windowed(windowSize2,1)
        .takeWhile { it.toSet().count() != windowSize2 }
        .count() + windowSize2
    println("part 2 answer: $result2")
}