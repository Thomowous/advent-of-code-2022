import java.io.File
import kotlin.math.absoluteValue

const val DECRYPTION_KEY = 811589153

class Node(val value: Long) {
    lateinit var prevNode: Node
    lateinit var nextNode: Node

    private fun remove() {
        this.prevNode.nextNode = this.nextNode
        this.nextNode.prevNode = this.prevNode
    }

    private fun insertAfter(node: Node) {
        this.prevNode = node
        this.nextNode = node.nextNode
        this.prevNode.nextNode = this
        this.nextNode.prevNode = this
    }

    fun shuffle(inputSize: Int) {
        val shift = this.value.absoluteValue % (inputSize - 1)
        if (this.value > 0) {
            var nodeToInsertAfter = this
            this.remove()
            repeat(shift.toInt()) {
                nodeToInsertAfter = nodeToInsertAfter.nextNode
            }
            this.insertAfter(nodeToInsertAfter)
        } else if (this.value < 0) {
            var nodeToInsertAfter = this
            this.remove()
            repeat(shift.toInt()) {
                nodeToInsertAfter = nodeToInsertAfter.prevNode
            }
            this.insertAfter(nodeToInsertAfter.prevNode)
        }
    }
}

fun createLinkedList(input: List<Long>, decryptionKey: Int): Pair<MutableList<Node>, Node> {
    val nodes = mutableListOf<Node>()

    var firstNode = Node(input[0])
    var prevNode = Node(input.last())
    var zeroNode = Node(0)

    input.forEachIndexed { index, value ->
        val currNode = Node(value * decryptionKey)
        prevNode.nextNode = currNode
        if (index == 0) {
            firstNode = currNode
        } else {
            currNode.prevNode = prevNode
        }
        if (value.toInt() == 0) {
            zeroNode = currNode
        }
        nodes.add(currNode)
        prevNode = currNode
    }

    prevNode.nextNode = firstNode
    firstNode.prevNode = prevNode

    return Pair(nodes, zeroNode)
}

fun getAnswer(zeroNode: Node): Long {
    var tmpNode = zeroNode
    var answer: Long = 0
    repeat(3) {
        repeat(1000) {
            tmpNode = tmpNode.nextNode
        }
        answer += tmpNode.value
    }
    return answer
}

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { it.toLong() }

    val (nodes1, zeroNode1) = createLinkedList(input, 1)
    val (nodes2, zeroNode2) = createLinkedList(input, DECRYPTION_KEY)

    nodes1.forEach { it.shuffle(nodes1.size) }
    repeat(10) { nodes2.forEach { it.shuffle(nodes2.size) } }

    println("part 1 answer: ${ getAnswer(zeroNode1) }")
    println("part 2 answer: ${ getAnswer(zeroNode2) }")
}