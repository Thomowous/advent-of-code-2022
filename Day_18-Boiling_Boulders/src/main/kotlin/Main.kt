import java.io.File
import java.util.LinkedList

const val ARRAY_SIZE = 25

fun getNeighbors(a: Int, b: Int, c: Int): List<Triple<Int, Int, Int>> {
    return listOf(
        Triple(a - 1, b, c),
        Triple(a + 1, b, c),
        Triple(a, b - 1, c),
        Triple(a, b + 1, c),
        Triple(a, b, c - 1),
        Triple(a, b, c + 1),
    )
}

fun checkOutOfBounds(a: Int, b: Int, c: Int, grid: Array<Array<Array<Boolean>>>, explored: MutableSet<Triple<Int, Int, Int>>) :Boolean {
    val neighbors = getNeighbors(a, b, c)

    neighbors.forEach { (x, y, z) ->
        if (x !in grid.indices) return true
        if (y !in grid.indices) return true
        if (z !in grid.indices) return true
    }
    val point = Triple(a, b, c)
    if (point in explored) return false
    explored.add(point)

    neighbors.forEach { (x, y, z) ->
        if (!grid[x][y][z]) {
            val gotOutOfBounds = checkOutOfBounds(x, y, z, grid, explored)
            if (gotOutOfBounds) return true
        }
    }
    return false
}

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { line -> line.split(',').map { it.toInt() } }
        .map { (a, b, c) -> Triple(a + 1, b + 1, c + 1) }

    val grid = Array(ARRAY_SIZE) { Array(ARRAY_SIZE) { Array(ARRAY_SIZE) { false } } }
    input.forEach { (a, b, c) ->
        grid[a][b][c] = true
    }

    var area1 = 0
    var area2 = 0

    val explored = Array(ARRAY_SIZE) { Array(ARRAY_SIZE) { Array(ARRAY_SIZE) { false } } }
    val exploredOutOfBounds = mutableSetOf<Triple<Int, Int, Int>>()

    val queue = LinkedList<Triple<Int, Int, Int>>()
    input.forEach { point ->
        queue.add(point)
        while (queue.size > 0) {
            val (a, b, c) = queue.pop()

            if (explored[a][b][c]) continue
            explored[a][b][c] = true

            getNeighbors(a, b, c).forEach { (x, y, z) ->
                if (!grid[x][y][z]) {
                    area1++

                    exploredOutOfBounds.clear()
                    if (checkOutOfBounds(x, y, z, grid, exploredOutOfBounds)) {
                        area2++
                    }
                } else if (!explored[x][y][z]) {
                    queue.add(Triple(x, y, z))
                }
            }
        }
    }

    println("part 1 answer: $area1")
    println("part 2 answer: $area2")
}

