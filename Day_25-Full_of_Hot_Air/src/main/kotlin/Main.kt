import java.io.File
import kotlin.math.absoluteValue

fun snafuToDecimal(snafu: String): Long {
    var decimal: Long = 0
    var multi: Long = 1
    snafu.reversed().forEach { char->
        decimal += multi * when(char) {
            '=' -> -2
            '-' -> -1
            '0' -> 0
            '1' -> 1
            '2' -> 2
            else -> 0
        }
        multi *= 5
    }
    return decimal
}

fun decimalToSnafu(decimal: Long, curr: Long, multi: Long): String {
    if (multi == 0L) {
        return ""
    }
    var snafu = ""
    var tmp = -2 * multi
    var closestMatch = tmp
    var coef = -2
    var tmpCoef = -2
    repeat(4) {
        tmp += multi
        tmpCoef++
        if ((decimal - (curr + tmp)).absoluteValue < (decimal - (curr + closestMatch)).absoluteValue) {
            closestMatch = tmp
            coef = tmpCoef
        }
    }
    snafu += when(coef) {
        -2 -> '='
        -1 -> '-'
        0 -> '0'
        1 -> '1'
        2 -> '2'
        else -> ""
    }
    snafu += decimalToSnafu(decimal, curr + closestMatch, multi / 5)

    return snafu
}

fun main() {
    val input = File("input.txt").readLines()

    val sum: Long = input.sumOf { snafuToDecimal(it) }

    var multi: Long = 1
    while (2 * multi < sum) {
        multi *= 5
    }

    val part1 = decimalToSnafu(sum, 0, multi)
    println("part 1 answer: $part1")
}