import java.io.File

class Monkey() {
    var inspects = arrayOf(0, 0)
    var items = Array<MutableList<Long>>(2) { mutableListOf() }
    var operation = ""
    var testDivisibility = 0
    var ifTrueTargetId = 0
    var ifFalseTargetId = 0

    private fun getWorryLevel(item: Long, divideBy: Int, modulo: Int): Long {
        // ["old", "+", "8"]
        var itemTmp = item
        val op = operation.replace("old", item.toString()).split(' ')
        if (op[1] == "+") {
            itemTmp += op[2].toLong()
        }
        if (op[1] == "*") {
            itemTmp *= op[2].toLong()
        }
        itemTmp /= divideBy
        itemTmp %= modulo
        return itemTmp
    }

    fun throwItems(monkeys: Array<Monkey>, modulo: Int): Array<Monkey> {
        val divisors = arrayOf(3, 1)
        for (part in 0 .. 1) {
            while(items[part].size > 0) {
                inspects[part]++
                val item = getWorryLevel(items[part].first(), divisors[part], modulo)
                items[part].removeFirst()
                if ((item % testDivisibility).toInt() == 0) {
                    monkeys[ifTrueTargetId].items[part].add(item)
                } else {
                    monkeys[ifFalseTargetId].items[part].add(item)
                }
            }
        }
        return monkeys
    }
}

fun main() {
    val input = File("input.txt")
        .readText()
        .split("\n\n")

    var monkeys = Array(input.size) { Monkey() }
    var modulo = 1
    input.forEach {
        val monkeyInput = it.split('\n')
        val monkeyId = monkeyInput[0].filter { c -> c.isDigit() }.toInt()
        // Starting items: 79, 98
        for (part in 0 .. 1) {
            monkeys[monkeyId].items[part] = monkeyInput[1]
                .split(',')
                .map { s -> s.filter { c -> c.isDigit() }.toLong() }
                .toMutableList()
        }
        // Operation: new = old * 19
        monkeys[monkeyId].operation = monkeyInput[2].split(" = ")[1]
        // Test: divisible by 23
        monkeys[monkeyId].testDivisibility = monkeyInput[3].filter { c -> c.isDigit() }.toInt()
        modulo *= monkeyInput[3].filter { c -> c.isDigit() }.toInt()
        // If true: throw to monkey 2
        monkeys[monkeyId].ifTrueTargetId = monkeyInput[4].filter { c -> c.isDigit() }.toInt()
        // If false: throw to monkey 3
        monkeys[monkeyId].ifFalseTargetId = monkeyInput[5].filter { c -> c.isDigit() }.toInt()
    }

    val numberOfRounds = 10_000
    for (round in 1 .. numberOfRounds) {
        monkeys.forEach { monkey ->
            monkeys = monkey.throwItems(monkeys, modulo)
        }
        if (round == 20) {
            val monkeysSorted = monkeys.toList().sortedByDescending { monkey -> monkey.inspects[0] }
            val monkeyBusiness = monkeysSorted[0].inspects[0] * monkeysSorted[1].inspects[0]
            println("part 1 answer: $monkeyBusiness")
            monkeys.forEach {monkey ->
                monkey.items[0].clear()
            }
        }
    }

    val monkeysSorted = monkeys.toList().sortedByDescending { monkey -> monkey.inspects[1] }
    val monkeyBusiness = monkeysSorted[0].inspects[1].toLong() * monkeysSorted[1].inspects[1].toLong()
    println("part 2 answer: $monkeyBusiness")
}