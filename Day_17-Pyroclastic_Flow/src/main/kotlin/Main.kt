import java.io.File

enum class Tile {
    AIR, ROCK
}

data class Point(var x: Int, var y: Int)

const val TURNS_PART_1: ULong = 2022u
const val TURNS_PART_2: ULong = 1_000_000_000_000u
const val CHAMBER_WIDTH = 7

fun renderToFile (chamber: MutableList<Array<Tile>>) {
    File("out.txt").printWriter().use { out ->
        chamber.filter { it.contains(Tile.ROCK) }.forEachIndexed { y, line ->
            out.println(line.map { if (it == Tile.ROCK) '#' else '.' }.joinToString(""))
        }
    }

}

fun main() {
    val gasDirections = File("input.txt").readText()

    val shapes = Array<MutableList<Array<Tile>>>(5) { mutableListOf() }
    val shapesInput = arrayOf(
        "####",
        ".#.\n###\n.#.",
        "..#\n..#\n###",
        "#\n#\n#\n#",
        "##\n##"
    )
    shapesInput.forEachIndexed { index, shapeString ->
        val shape = mutableListOf<Array<Tile>>()
        shapeString.split('\n').forEach { shapeLineString ->
            val shapeLine = Array(shapeLineString.length) { Tile.AIR }
            val asd = shapeLineString.map { if (it == '#') Tile.ROCK else Tile.AIR }
            asd.forEachIndexed { x, tile ->
                shapeLine[x] = tile
            }
            shape.add(shapeLine)
        }
        shapes[index] = shape.reversed().toMutableList()
    }

    val chamber = MutableList(0) { Array(CHAMBER_WIDTH) { Tile.AIR } }

    // text files were made by manually
    val startHeight = File("start.txt").readLines().size.toULong()
    val patternHeight = File("pattern.txt").readLines().size.toULong()
    val endHeight = File("end.txt").readLines().size.toULong()
    var turnInPattern: ULong = 0u // from calculator

    var startTurns: ULong = 0u
    var patternTurns: ULong = 0u

    var maxY = 0
    var turn: ULong = 0u
    var shapeCounter = 0
    var gasCounter = 0
    while (turn < TURNS_PART_2) {
        if (turn == TURNS_PART_1) {
            println("part 1 answer: $maxY")
        }
        while (chamber.size < (maxY + 10)) {
            chamber.add(Array(CHAMBER_WIDTH) { Tile.AIR })
        }

        val shapePosition = Point(2, maxY + 3)
        val shape = shapes[shapeCounter]
        shapeCounter = (shapeCounter + 1) % shapes.size
        if (turn < 1_000_000u) {
            renderToFile(chamber)
            if (File("out.txt").readText() == File("start.txt").readText()) {
                startTurns = turn
            }
            if (File("out.txt").readText() == File("start_pattern.txt").readText()) {
                patternTurns = turn - startTurns
                turnInPattern = TURNS_PART_2 / patternTurns
                turn = startTurns + patternTurns * turnInPattern
            }
        }
        while(true) {
            val gasDirection = gasDirections[gasCounter]
            gasCounter = (gasCounter + 1) % gasDirections.length
            if (gasDirection == '<') {
                var canMoveLeft = true
                shape.forEachIndexed { y, shapeLine ->
                    shapeLine.forEachIndexed { x, tile ->
                        if (shapePosition.x + x - 1 < 0) {
                            canMoveLeft = false
                        } else if (tile == Tile.ROCK && chamber[shapePosition.y + y][shapePosition.x + x - 1] == Tile.ROCK) {
                            canMoveLeft = false
                        }
                    }
                }
                if (canMoveLeft) {
                    shapePosition.x--
                }
            } else {
                var canMoveRight = true
                shape.forEachIndexed { y, shapeLine ->
                    shapeLine.forEachIndexed { x, tile ->
                        if (shapePosition.x + x + 1 >= CHAMBER_WIDTH) {
                            canMoveRight = false
                        } else if (tile == Tile.ROCK && chamber[shapePosition.y + y][shapePosition.x + x + 1] == Tile.ROCK) {
                            canMoveRight = false
                        }
                    }
                }
                if (canMoveRight) {
                    shapePosition.x++
                }
            }
            var canFall = true
            shape.forEachIndexed { y, shapeLine ->
                shapeLine.forEachIndexed { x, tile ->
                    if (shapePosition.y + y - 1 < 0) {
                        canFall = false
                    } else if (tile == Tile.ROCK && chamber[shapePosition.y + y - 1][shapePosition.x + x] == Tile.ROCK) {
                        canFall = false
                    }
                }
            }
            if (!canFall) {
                break
            }
            shapePosition.y--
        }
        shape.forEachIndexed { y, shapeLine ->
            shapeLine.forEachIndexed { x, tile ->
                if (tile == Tile.ROCK) {
                    chamber[shapePosition.y + y][shapePosition.x + x] = Tile.ROCK
                }
            }
        }
        maxY = chamber.indexOfLast { it.contains(Tile.ROCK) } + 1
        turn++
    }
    val result2 = startHeight + patternHeight * turnInPattern + endHeight
    println("part 2 answer: $result2")
}