import java.io.File
import java.lang.Exception
import java.util.LinkedList

const val TIME_PART_1 = 30
const val TIME_PART_2 = 26
const val STARTING_VALVE = "AA"

data class Valve(val name: String, val flowRate: Int) {
    var leadsTo = mutableMapOf<Valve, Int>()
}

data class State(
    val currentValve: Valve,
    val remainingValves: List<Valve>,
    val path: Set<Valve>,
    val pressure: Int,
    val timeRemaining: Int
)

fun getShortestPath(fromValve: Valve, toValve: Valve): Int {
    val queue = LinkedList<Pair<Valve, Int>>()
    val visited = mutableListOf(fromValve)
    queue.add(Pair(fromValve, 0))
    while (queue.size > 0) {
        val (valve, distance) = queue.poll()
        if (valve == toValve) {
            return distance
        }
        valve.leadsTo.forEach {
            if (it.value == 1 && it.key !in visited) {
                visited.add(it.key)
                queue.add(Pair(it.key, distance + it.value))
            }
        }
    }
    throw Exception("path not found")
}

fun getInput(): Valve {
    val input = File("input.txt")
        .readLines()
        .map { it.split(';') }
        .associate { (valve, tunnels) ->
            val name = valve.substring(6, 8)
            val flowRate = valve.filter { it.isDigit() }.toInt()
            val leadsTo = tunnels.filter { it.isUpperCase() }.chunked(2).toMutableList()
            name to Pair(Valve(name, flowRate), leadsTo)
        }

    var valves = mutableMapOf<String, Valve>()
    input.forEach forEach1@ { (name, valveData) ->
        val valve = valveData.first
        valveData.second.forEach { otherValveName ->
            valve.leadsTo[input[otherValveName]!!.first] = 1
        }
        valves[name] = valve
    }

    // get distance between every valve combination
    valves.filterValues { it.flowRate > 0 }.forEach forEachFrom@  { (valveNameFrom, valveFrom) ->
        valves.filterValues { it.flowRate > 0 }.forEach forEachTo@ { (valveNameTo, valveTo) ->
            if (valveNameFrom == valveNameTo) return@forEachTo
            val valveDistance = getShortestPath(valveFrom, valveTo)
            valveFrom.leadsTo[valveTo] = valveDistance
            valveTo.leadsTo[valveFrom] = valveDistance
        }
    }

    // set starting value
    val startingValve = valves[STARTING_VALVE]!!
    valves = valves.filter { it.value.flowRate > 0 }.toMutableMap()
    valves.forEach { (name, valve) ->
        startingValve.leadsTo[valve] = getShortestPath(startingValve, valve)
    }

    val zeroFlowValveNeighbors = mutableListOf<Valve>()

    // remove useless valves leading from valves
    valves.forEach { (_, valve) ->
        zeroFlowValveNeighbors.clear()
        valve.leadsTo.forEach { (valveNeighbor, _) ->
            if (valveNeighbor.flowRate == 0) {
                zeroFlowValveNeighbors.add(valveNeighbor)
            }
        }
        zeroFlowValveNeighbors.forEach { valve.leadsTo.remove(it) }
    }

    // remove useless valves leading from starting valve
    zeroFlowValveNeighbors.clear()
    startingValve.leadsTo.forEach { (valveNeighbor, _) ->
        if (valveNeighbor.flowRate == 0) {
            zeroFlowValveNeighbors.add(valveNeighbor)
        }
    }
    zeroFlowValveNeighbors.forEach { startingValve.leadsTo.remove(it) }
    return startingValve
}

fun getStatesBFS(queue: LinkedList<State>): MutableSet<State> {
    val states = mutableSetOf<State>()
    while(queue.size > 0) {
        val state = queue.pop()
        if (state.timeRemaining < 0) {
            continue
        }
        states.add(state)
        state.remainingValves.forEach { valve ->
            val newRemaining = state.remainingValves.toMutableList()
            newRemaining.remove(state.currentValve)
            val newPath = state.path.toMutableSet()
            newPath.add(state.currentValve)
            val distance = state.currentValve.leadsTo[valve]
            val timeRemaining = if (distance == null) 0 else state.timeRemaining - distance - 1
            queue.add(State(
                valve,
                newRemaining,
                newPath,
                state.pressure + state.currentValve.flowRate * (state.timeRemaining - 1),
                timeRemaining
            ))
        }
    }
    return states
}

fun main() {
    val startTime = System.nanoTime()

    val startingValve = getInput()

    // PART 1
    var queue = LinkedList<State>()
    startingValve.leadsTo.forEach { (valve, distance) ->
        queue.add(State(valve, valve.leadsTo.keys.toList(), setOf(), 0, TIME_PART_1 - distance))
    }
    var states = getStatesBFS(queue)

    val midTime = System.nanoTime()
    println("part 1 answer: ${ states.maxBy { it.pressure }.pressure } (done in ${ (midTime - startTime) / 1_000_000 }ms)")

    // PART 2
    queue = LinkedList<State>()
    startingValve.leadsTo.forEach { (valve, distance) ->
        queue.add(State(valve, valve.leadsTo.keys.toList(), setOf(), 0, TIME_PART_2 - distance))
    }
    states = getStatesBFS(queue)

    var bestPressure = 0
    states.forEachIndexed { i, state1 ->
        states.drop(i).forEach { state2 ->
            if (state1.pressure + state2.pressure > bestPressure && state1.path.intersect(state2.path).isEmpty()) {
                bestPressure = state1.pressure + state2.pressure
            }
        }
    }

    val endTime = System.nanoTime()
    println("part 2 answer: $bestPressure (done in ${ (endTime - midTime) / 1_000_000 }ms)")
}