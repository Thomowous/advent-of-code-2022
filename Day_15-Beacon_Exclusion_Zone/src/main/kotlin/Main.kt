import java.io.File
import kotlin.math.absoluteValue

const val SEARCH_Y = 2_000_000
const val MAX_COORDINATE = 4_000_000
const val TUNING_SIGNAL_MULTIPLIER = 4_000_000u

data class Point(var x: Int, var y: Int)

fun pointIsInRangeOfSensors(point: Point, input: List<List<Point>>, distances: Array<Int>): Boolean {
    input.forEachIndexed { index, (sensor, beacon) ->
        val distanceST = (sensor.x - point.x).absoluteValue + (sensor.y - point.y).absoluteValue
        val distanceSB = distances[index]
        if (distanceST <= distanceSB) {
            return true
        }
    }
    return false
}

fun main() {
    val startTime = System.nanoTime()
    val input = File("input.txt")
        .readLines()
        .map { it.split(", ", ": ") }
        .map { it.map { it.filter { it == '-' || it.isDigit() }.toInt() } }
        .map { (sX, sY, bX, bY) -> listOf(Point(sX, sY), Point(bX, bY)) }

    val distances = Array(input.size) { 0 }
    input.forEachIndexed { i, (sensor, beacon) ->
        distances[i] = (sensor.x - beacon.x).absoluteValue + (sensor.y - beacon.y).absoluteValue
    }

    // part 1
    val searchedXs = mutableSetOf<Int>()
    input.forEachIndexed { i, (sensor, _) ->
        val distance = distances[i]
        if (SEARCH_Y !in sensor.y ..  sensor.y + distance && SEARCH_Y !in sensor.y - distance .. sensor.y) {
            return@forEachIndexed
        }
        var xOffset = 0
        val steps = distance - (SEARCH_Y - sensor.y).absoluteValue
        repeat(steps + 1) {
            searchedXs.add(sensor.x + xOffset)
            searchedXs.add(sensor.x - xOffset)
            xOffset++
        }
    }

    input.forEach { (_, beacon) ->
        if (beacon.y == SEARCH_Y) {
            searchedXs.remove(beacon.x)
        }
    }
    val endTimeP1 = System.nanoTime()

    // part 2
    val points = mutableListOf<Point>()
    input.forEachIndexed { index, (sensor, _) ->
        val distance = distances[index]
        for (i in 0 .. distance) {
            if (sensor.x + i in 0..MAX_COORDINATE && sensor.y + distance - i + 1 in 0 .. MAX_COORDINATE) {
                points.add(Point(sensor.x + i, sensor.y + distance - i + 1))
            }
            if (sensor.x - i in 0..MAX_COORDINATE && sensor.y + distance - i + 1 in 0 .. MAX_COORDINATE) {
                points.add(Point(sensor.x - i, sensor.y + distance - i + 1))
            }
            if (sensor.x + i in 0..MAX_COORDINATE && sensor.y - distance + i - 1 in 0 .. MAX_COORDINATE) {
                points.add(Point(sensor.x + i, sensor.y - distance + i - 1))
            }
            if (sensor.x - i in 0..MAX_COORDINATE && sensor.y - distance + i - 1 in 0 .. MAX_COORDINATE) {
                points.add(Point(sensor.x - i, sensor.y - distance + i - 1))
            }
        }
    }

    var distressBeacon = Point(0, 0)
    while (points.size > 1) {
        val testPoint = points.last()
        if (!pointIsInRangeOfSensors(testPoint, input, distances)) {
            distressBeacon = testPoint
            break
        }
        points.removeLast()

    }
    val endTimeP2 = System.nanoTime()


    // results
    println("part 1 answer: ${ searchedXs.size }")
    println("part 1 done in: ${ (endTimeP1 - startTime) / 1_000_000 }ms")
    println()

    val tuningFrequency = distressBeacon.x.toULong() * TUNING_SIGNAL_MULTIPLIER + distressBeacon.y.toULong()
    println("part 2 answer: $tuningFrequency")
    println("part 2 done in: ${ (endTimeP2 - endTimeP1) / 1_000_000 }ms")
}