import java.io.File

enum class Tile {
    AIR, ROCK, SAND
}

data class Point(var x: Int, var y: Int)

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { line -> line.split(" -> ")
            .map { it.split(",") }
            .map { (x, y) -> Point(x.toInt(), y.toInt()) }
        }

    val maxY = input.flatMap { it.map { (_, y) -> y } }.max() + 2
    val minX = input.flatMap { it.map { (x, _) -> x } }.min() - 150
    val maxX = input.flatMap { it.map { (x, _) -> x } }.max() + 150
    val cave = Array(maxY + 1) { Array(maxX + 1) { Tile.AIR } }

    input.forEach {
        it.windowed(2, 1).forEach {(pair1, pair2) ->
            val localMinY = pair1.y.coerceAtMost(pair2.y)
            val localMaxY = pair1.y.coerceAtLeast(pair2.y)
            for (y in localMinY .. localMaxY) {
                cave[y][pair1.x] = Tile.ROCK
            }
            val localMinX = pair1.x.coerceAtMost(pair2.x)
            val localMaxX = pair1.x.coerceAtLeast(pair2.x)
            for (x in localMinX .. localMaxX) {
                cave[pair1.y][x] = Tile.ROCK
            }
        }
    }
    for (x in minX .. maxX) {
        cave[maxY][x] = Tile.ROCK
    }
    var sand = Point(500, 0)
    var sandCounter = 0
    var part1 = 0
    while (true) {
        if (cave[sand.y][sand.x] == Tile.SAND) {
            break
        }
        if (sand.y + 1 > maxY - 2 && part1 == 0) {
            part1 = sandCounter
        }
        // check down
        if (cave[sand.y + 1][sand.x] == Tile.AIR) {
            sand.y++
            continue
        }
        // check down-left
        if (cave[sand.y + 1][sand.x - 1] == Tile.AIR) {
            sand.y++
            sand.x--
            continue
        }
        // check down-right
        if (cave[sand.y + 1][sand.x + 1] == Tile.AIR) {
            sand.y++
            sand.x++
            continue
        }
        cave[sand.y][sand.x] = Tile.SAND
        sandCounter++
        sand = Point(500, 0)
    }

    cave.forEach { line ->
        line.forEachIndexed { index, tile ->
            if (index < minX) return@forEachIndexed
            if (tile == Tile.AIR) print(' ')
            if (tile == Tile.ROCK) print('#')
            if (tile == Tile.SAND) print('o')
        }
        println()
    }

    println("part 1 answer: $part1")
    println("part 2 answer: $sandCounter")
}