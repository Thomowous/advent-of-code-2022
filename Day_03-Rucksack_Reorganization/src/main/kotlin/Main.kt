import java.io.File

fun String.splitAtIndex(index:Int): List<String> {
    return listOf(this.substring(0, index), this.substring(index, this.length))
}
fun List<List<String>>.getUniqueCharsInLists(): List<Set<Char>> {
    return this
        .map { groups -> groups.map { it.toSet() } }
        .map { groups -> groups.reduce { acc, a -> acc.intersect(a) } }
}

fun main() {
    val alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    val input = File("input.txt").readLines()

    val resultPart1 = input
        .map { it.splitAtIndex(it.length / 2) }
        .getUniqueCharsInLists()
        .sumOf { alphabet.indexOf(it.first()) + 1 }
    println("part 1 answer: $resultPart1")

    val resultPart2 = input
        .windowed(3, 3)
        .getUniqueCharsInLists()
        .sumOf { alphabet.indexOf(it.first()) + 1 }
    println("part 2 answer: $resultPart2")
}