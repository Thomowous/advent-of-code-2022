import java.io.File

enum class Tile() {
    Wall,
    Empty,
    Void,
}


class Position(var x: Int, var y: Int, var facing: Int) {
    var currentCubeFaceIndex = 0

    fun turn(turn: Char) {
        if (turn == 'L') {
            facing = (facing - 1).mod(4)
        }
        if (turn == 'R') {
            facing = (facing + 1).mod(4)
        }
    }
    private fun getDirection(): Pair<Int, Int> {
        if (facing == 0) return Pair(1, 0)
        if (facing == 1) return Pair(0, 1)
        if (facing == 2) return Pair(-1, 0)
        if (facing == 3) return Pair(0, -1)
        return Pair(0, 0)
    }
    fun move(count: Int, map: List<List<Tile>>) {
        val (xMove, yMove) = getDirection()
        repeat(count) {
            var newX = (x + xMove).mod(map[0].size)
            var newY = (y + yMove).mod(map.size)
            if (map[newY][newX] == Tile.Wall) return

            while (map[newY][newX] == Tile.Void) {
                newX = (newX + xMove).mod(map[0].size)
                newY = (newY + yMove).mod(map.size)
                if (map[newY][newX] == Tile.Wall) return
            }
            x = newX
            y = newY
        }
    }
    fun moveCube(count: Int, map: CubeMap) {
        repeat(count) {
            val (xMove, yMove) = getDirection()
            var cubeFace = map.cubeFaces[currentCubeFaceIndex]
            val lastIndex = cubeFace.tiles.lastIndex
            var newX = (x + xMove)
            var newY = (y + yMove)
            var newCubeFaceIndex = currentCubeFaceIndex
            var newFacing = facing

            if (newX !in cubeFace.tiles.indices) {
                if (newX < 0) {
                    val newCubeFace = cubeFace.left
                    if (newCubeFace.right == cubeFace) {
                        newFacing = 2
                        newX = lastIndex
                    }
                    if (newCubeFace.left == cubeFace) {
                        newFacing = 0
                        newX = 0
                        newY = lastIndex - newY
                    }
                    if (newCubeFace.up == cubeFace) {
                        newFacing = 1
                        newX = newY
                        newY = 0
                    }
                    if (newCubeFace.down == cubeFace) {
                        newFacing = 3
                        newX = lastIndex - newY
                        newY = lastIndex
                    }
                    newCubeFaceIndex = map.cubeFaces.indexOf(newCubeFace)
                }
                if (newX > lastIndex) {
                    val newCubeFace = cubeFace.right
                    if (newCubeFace.left == cubeFace) {
                        newFacing = 0
                        newX = 0
                    }
                    if (newCubeFace.right == cubeFace) {
                        newFacing = 2
                        newX = lastIndex
                        newY = lastIndex - newY
                    }
                    if (newCubeFace.up == cubeFace) {
                        newFacing = 1
                        newX = lastIndex - newY
                        newY = 0
                    }
                    if (newCubeFace.down == cubeFace) {
                        newFacing = 3
                        newX = newY
                        newY = lastIndex
                    }
                    newCubeFaceIndex = map.cubeFaces.indexOf(newCubeFace)
                }

            }
            if (newY !in cubeFace.tiles.indices) {
                if (newY < 0) {
                    val newCubeFace = cubeFace.up
                    if (newCubeFace.down == cubeFace) {
                        newFacing = 3
                        newY = lastIndex
                    }
                    if (newCubeFace.up == cubeFace) {
                        newFacing = 1
                        newX = lastIndex - newY
                        newY = 0
                    }
                    if (newCubeFace.left == cubeFace) {
                        newFacing = 0
                        newY = newX
                        newX = 0
                    }
                    if (newCubeFace.right == cubeFace) {
                        newFacing = 2
                        newY = newX
                        newX = lastIndex
                    }
                    newCubeFaceIndex = map.cubeFaces.indexOf(newCubeFace)
                }
                if (newY > lastIndex) {
                    val newCubeFace = cubeFace.down
                    if (newCubeFace.up == cubeFace) {
                        newFacing = 1
                        newY = 0
                    }
                    if (newCubeFace.down == cubeFace) {
                        newFacing = 3
                        newX = lastIndex - newX
                        newY = lastIndex
                    }
                    if (newCubeFace.left == cubeFace) {
                        newY = lastIndex - newX
                        newX = 0
                    }
                    if (newCubeFace.right == cubeFace) {
                        newFacing = 2
                        newY = newX
                        newX = lastIndex
                    }
                    newCubeFaceIndex = map.cubeFaces.indexOf(newCubeFace)
                }

            }
            cubeFace = map.cubeFaces[newCubeFaceIndex]
            if (cubeFace.tiles[newY][newX] == Tile.Wall) {
                return
            }

            x = newX
            y = newY
            currentCubeFaceIndex = newCubeFaceIndex
            facing = newFacing
        }
    }
    override fun toString(): String {
        return "x: $x, y: $y, facing: $facing, cubeFace: $currentCubeFaceIndex"
    }
}

data class CubeFace(val tiles: List<List<Tile>>) {
    lateinit var left: CubeFace
    lateinit var right: CubeFace
    lateinit var up: CubeFace
    lateinit var down: CubeFace
}

class CubeMap() {
    var cubeFaces = mutableListOf<CubeFace>()

    constructor(monkeyMap: List<List<Tile>>) : this() {
        val cubeFaces = mutableListOf<CubeFace>()
        val faceSize = 50
        val maxY = 4
        val maxX = 3
        val faceMap = mutableListOf<List<List<List<Tile>>>>()

        for (y in 0 until maxY) {
            val tmpFaceMap = mutableListOf<List<List<Tile>>>()
            for (x in 0 until  maxX) {
                val tmp = mutableListOf<List<Tile>>()
                for (tileY in y * faceSize until (y + 1) * faceSize) {
                    val tmpX = mutableListOf<Tile>()
                    for (tileX in x * faceSize until (x + 1) * faceSize) {
                        tmpX.add(monkeyMap[tileY][tileX])
                    }
                    tmp.add(tmpX)
                }
                tmpFaceMap.add(tmp)
            }
            faceMap.add(tmpFaceMap)
        }

        faceMap.forEach { yChunks ->
            yChunks.forEach {
                if (it[0][0] != Tile.Void) {
                    cubeFaces.add(CubeFace(it))
                }
            }
        }

        cubeFaces[0].left = cubeFaces[3]
        cubeFaces[0].right = cubeFaces[1]
        cubeFaces[0].up = cubeFaces[5]
        cubeFaces[0].down = cubeFaces[2]

        cubeFaces[1].left = cubeFaces[0]
        cubeFaces[1].right = cubeFaces[4]
        cubeFaces[1].up = cubeFaces[5]
        cubeFaces[1].down = cubeFaces[2]

        cubeFaces[2].left = cubeFaces[3]
        cubeFaces[2].right = cubeFaces[1]
        cubeFaces[2].up = cubeFaces[0]
        cubeFaces[2].down = cubeFaces[4]

        cubeFaces[3].left = cubeFaces[0]
        cubeFaces[3].right = cubeFaces[4]
        cubeFaces[3].up = cubeFaces[2]
        cubeFaces[3].down = cubeFaces[5]

        cubeFaces[4].left = cubeFaces[3]
        cubeFaces[4].right = cubeFaces[1]
        cubeFaces[4].up = cubeFaces[2]
        cubeFaces[4].down = cubeFaces[5]

        cubeFaces[5].left = cubeFaces[0]
        cubeFaces[5].right = cubeFaces[4]
        cubeFaces[5].up = cubeFaces[3]
        cubeFaces[5].down = cubeFaces[1]

        this.cubeFaces = cubeFaces
    }
}

fun main() {
    val charToWall = mapOf(' ' to Tile.Void, '.' to Tile.Empty, '#' to Tile.Wall)

    val input = File("input.txt")
        .readLines()

    val pathMoves = input.last().split('L', 'R').map { it.toInt() }
    val pathTurns = input.last().filter { it.isLetter() }

    val maxLength = input.dropLast(2).maxOf { it.length }
    val map = input.dropLast(2)
        .map { it.padEnd(maxLength, ' ') }
        .map { it.map { char -> charToWall[char]!! } }

    val x = map[0].indexOfFirst { it == Tile.Empty }
    var currentPosition = Position(x, 0, 0)

    pathMoves.forEachIndexed { index, move ->
        currentPosition.move(move, map)
        if (index != pathMoves.lastIndex) {
            currentPosition.turn(pathTurns[index])
        }
    }
    val part1 = 1000 * (currentPosition.y + 1) + 4 * (currentPosition.x + 1) + currentPosition.facing
    println(part1)


    val cubeMap = CubeMap(map)
    currentPosition = Position(0, 0, 0)
    pathMoves.forEachIndexed { index, move ->
        currentPosition.moveCube(move, cubeMap)
        if (index != pathMoves.lastIndex) {
            currentPosition.turn(pathTurns[index])
        }
    }
    var yMulti = 0
    if (currentPosition.currentCubeFaceIndex == 2) yMulti = 1
    if (currentPosition.currentCubeFaceIndex == 3) yMulti = 2
    if (currentPosition.currentCubeFaceIndex == 4) yMulti = 2
    if (currentPosition.currentCubeFaceIndex == 5) yMulti = 3

    var xMulti = 0
    if (currentPosition.currentCubeFaceIndex == 0) xMulti = 1
    if (currentPosition.currentCubeFaceIndex == 2) xMulti = 1
    if (currentPosition.currentCubeFaceIndex == 4) xMulti = 1
    if (currentPosition.currentCubeFaceIndex == 1) xMulti = 2

    val part2 = 1000 * (50 * yMulti + currentPosition.y + 1) + 4 * (50 * xMulti + currentPosition.x + 1) + currentPosition.facing
    println(part2)
}
