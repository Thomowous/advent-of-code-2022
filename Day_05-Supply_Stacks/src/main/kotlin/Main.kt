import java.io.File

fun main() {
    val input = File("input.txt").readText().split("\n\n")
    val numberOfContainers = input[0].trim().takeLastWhile { it.isDigit() }.toInt()

    val stacksPart1 = input[0]
        .split('\n')
        .dropLast(1)
        .map { line -> line.chunked(4).map { it[1] } }
        .fold(Array(numberOfContainers) { "" }) { acc, chars ->
            chars.forEachIndexed { index, c -> if (c != ' ') acc[index] = c + acc[index] }
            acc
        }
    val stacksPart2 = Array(numberOfContainers) { stacksPart1[it] }

    val instructions = input[1].split('\n').map { step -> step.split(' ').mapNotNull { it.toIntOrNull() } }
    val result1 = instructions.fold(stacksPart1) { stacks, (count, from, to) ->
        stacks[to - 1] += stacks[from - 1].takeLast(count).reversed()
        stacks[from - 1] = stacks[from - 1].dropLast(count)
        stacks
    }.map { it.last() }.joinToString("")
    println("part 1 answer: $result1")

    val result2 = instructions.fold(stacksPart2) { stacks, (count, from, to) ->
        stacks[to - 1] += stacks[from - 1].takeLast(count)
        stacks[from - 1] = stacks[from - 1].dropLast(count)
        stacks
    }.map { it.last() }.joinToString("")
    println("part 2 answer: $result2")
}