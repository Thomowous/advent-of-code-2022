import java.io.File
import kotlin.math.min

class Directory(var parent: Directory?, var children: MutableList<Directory>, var size: Int) {
    fun getFullSize(): Int {
        return size + children.sumOf { it.getFullSize() }
    }

    fun part1(maxSize: Int): Int {
        var tmpSize = getFullSize()
        if (tmpSize > maxSize) {
            tmpSize = 0
        }
        return tmpSize + children.sumOf { it.part1(maxSize) }
    }

    fun part2(requiredSpace: Int, minDirectorySize: Int): Int {
        val fullSize = getFullSize()
        if (fullSize < requiredSpace) return minDirectorySize
        val childrenMin = children.minOf { it.part2(requiredSpace, minDirectorySize) }
        return minOf(minDirectorySize, childrenMin, fullSize)
    }
}

fun main() {
    val input = File("input.txt").readLines()

    // generate tree
    val rootDirectory = Directory(null, mutableListOf(), 0)
    var currentDirectory = rootDirectory

    for (i in input.indices) {
        val line = input[i].split(' ')

        // dir change
        if (line[0] == "$" && line[1] == "cd") {
            if (line[2] == "..") {
                // cd out
                currentDirectory = currentDirectory.parent!!
            } else {
                // make new directory and go into it
                currentDirectory = Directory(currentDirectory, mutableListOf(), 0)
                currentDirectory.parent!!.children.add(currentDirectory)
            }
        }
        if (line[0] == "$" || line[0] == "dir") continue

        currentDirectory.size += line[0].toInt()
    }

    val maxDirectorySize = 100_000
    println("part 1 answer: ${ rootDirectory.part1(maxDirectorySize) }")

    val totalSpace = 70_000_000
    val unusedSpace = totalSpace - rootDirectory.getFullSize()
    val updateSpace = 30_000_000
    val requiredSpace = updateSpace - unusedSpace
    println("part 2 answer: ${ rootDirectory.part2(requiredSpace, Int.MAX_VALUE) }")
}