import java.awt.Point
import java.io.File
import kotlin.math.absoluteValue
import kotlin.math.sign

fun moveHead(head: Point, direction: String): Point {
    return when(direction) {
        "L" -> Point(head.x - 1, head.y)
        "R" -> Point(head.x + 1, head.y)
        "U" -> Point(head.x, head.y + 1)
        "D" -> Point(head.x, head.y - 1)
        else -> Point(head.x, head.y)
    }
}

fun moveRope(rope: MutableList<Point>, direction: String): MutableList<Point> {
    rope[0] = moveHead(rope[0], direction)

    for (i in 0 until rope.lastIndex) {
        val difference = Point(rope[i + 1].x - rope[i].x, rope[i + 1].y - rope[i].y)
        // don't move if the knots are touching
        if (difference.x.absoluteValue <= 1 && difference.y.absoluteValue <= 1) continue
        // move otherwise
        rope[i + 1] = Point(rope[i + 1].x - difference.x.sign, rope[i + 1].y - difference.y.sign)
    }
    return rope
}

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { it.split(' ') }

    var rope = MutableList(10) { Point(0, 0) }
    val visited2 = mutableSetOf(Point(0, 0))
    val visited1 = mutableSetOf(Point(0, 0))

    input.forEach { (direction, distance) ->
        for (i in 0 until distance.toInt()) {
            rope = moveRope(rope, direction)
            visited1.add(rope[1])
            visited2.add(rope.last())
        }
    }
    println("part 1 answer: ${ visited1.count() }")
    println("part 2 answer: ${ visited2.count() }")
}