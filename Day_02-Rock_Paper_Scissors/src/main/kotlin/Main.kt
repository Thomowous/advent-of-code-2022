import java.io.File

fun <T> Boolean.elvis( a :T, b :T ): T{
    if (this) // this here refers to the boolean result
        return a
    return b
}

fun getRoundPointsPart1(round: List<Int>) : Int {
    // round[0] is elf
    // round[1] is me
    if (round[0] == round[1]) {
        return 3 + round[1] // draw
    }
    if (Math.floorMod(round[1] - round[0] - 1, 3) == 0) {
        return 6 + round[1] // win
    }
    return 0 + round[1] // loss
}

fun getRoundPointsPart2(round: List<Int>) : Int {
    // round[0] is elf
    // round[1] is me
    if (round[1] == 1) {
        return 0 + (round[0] - 1 == 0).elvis(3, round[0] - 1) // loss
    }
    if (round[1] == 2) {
        return 3 + round[0] // draw
    }
    return 6 + (round[0] + 1 == 4).elvis(1, round[0] + 1) // win
}

fun main() {
    val dictionary = mapOf("A" to 1, "B" to 2, "C" to 3, "X" to 1, "Y" to 2, "Z" to 3)

    val input = File("input.txt")
        .readText()
        .split('\n')
        .map { round -> round.split(' ').map { dictionary[it] ?: 0 } }

    val resultPart1 = input.sumOf { getRoundPointsPart1(it) }
    println("part 1 answer: $resultPart1")

    val resultPart2 = input.sumOf { getRoundPointsPart2(it) }
    println("part 2 answer: $resultPart2")
}