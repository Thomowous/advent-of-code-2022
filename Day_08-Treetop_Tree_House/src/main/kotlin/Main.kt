import java.io.File

fun getScenicScore(input: List<List<Int>>, startX: Int, startY: Int): Int {
    val tree = input[startY][startX]
    val scores = arrayOf(0,0,0,0)

    for (x in (startX + 1) until input[startY].size) {
        scores[0]++
        if (input[startY][x] >= tree) break
    }
    for (x in (startX - 1).downTo(0)) {
        scores[1]++
        if (input[startY][x] >= tree) break
    }
    for (y in startY + 1 until input.size) {
        scores[2]++
        if (input[y][startX] >= tree) break
    }
    for (y in (startY - 1).downTo(0)) {
        scores[3]++
        if (input[y][startX] >= tree) break
    }

    return scores.reduce { acc, i -> acc * i }
}

fun main() {
    val input = File("input.txt")
        .readLines()
        .map { line -> line.map { it.digitToInt() } }

    val result1 = Array(input.size) { Array(input[it].size) { 0 } }
    var result2 = 0

    var maxLeft = -1
    var maxRight = -1
    for (y in input.indices) {
        for (x in input[y].indices) {
            result1[y][x] += input[y][x].compareTo(maxLeft).coerceAtLeast(0)
            result1[y][result1[y].size - x - 1] += input[y][input[y].size - x - 1].compareTo(maxRight).coerceAtLeast(0)

            maxLeft = maxLeft.coerceAtLeast(input[y][x])
            maxRight = maxRight.coerceAtLeast(input[y][input[y].size - x - 1])

            result2 = result2.coerceAtLeast(getScenicScore(input, x, y))
        }
        maxLeft = -1
        maxRight = -1
    }

    var maxTop = -1
    var maxBottom = -1
    for (x in input[0].indices) {
        for (y in input.indices) {
            result1[y][x] += input[y][x].compareTo(maxTop).coerceAtLeast(0)
            result1[result1.size - y - 1][x] += input[input.size - y - 1][x].compareTo(maxBottom).coerceAtLeast(0)

            maxTop = maxTop.coerceAtLeast(input[y][x])
            maxBottom = maxBottom.coerceAtLeast(input[input.size - y - 1][x])
        }
        maxTop = -1
        maxBottom = -1
    }

    println("part 1 answer: ${ result1.sumOf { line -> line.count { it > 0 } } }")
    println("part 2 answer: $result2")
}