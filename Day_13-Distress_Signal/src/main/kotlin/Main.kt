import java.io.File

fun processInputLine(line: String): Pair<MutableList<Any>, Int> {
    val list = mutableListOf<Any>()
    var i = 0
    var numBuffer = ""
    while (i < line.length) {
        if (line[i] == '[') {
            val (subList, endIndex) = (processInputLine(line.substring(i + 1)))
            list.add(subList)
            i += endIndex + 1
        } else if (line[i] == ']') {
            if (numBuffer != "") {
                list.add(numBuffer.toInt())
            }
            return Pair(list, i)
        } else if (line[i].isDigit()) {
            numBuffer += line[i]
        } else if (line[i] == ',') {
            if (numBuffer != "") {
                list.add(numBuffer.toInt())
                numBuffer = ""
            }
        }

        i++

    }
    return Pair(list, line.length)
}

fun checkPacketPair(left: MutableList<Any>, right: MutableList<Any>): Boolean? {
    var i = 0
    var isCorrectOrder: Boolean? = null
    while (true) {
        // index check
        if (i !in left.indices) {
            if (i in right.indices) {
                return true
            }
            return null
        }
        if (i !in right.indices) {
            return false
        }
        // both int check
        if (left[i] is Int && right[i] is Int) {
            if ((left[i] as Int) < (right[i] as Int)) {
                return true
            }
            if ((left[i] as Int) > (right[i] as Int)) {
                return false
            }
        // both list check
        } else if (left[i] is MutableList<*> && right[i] is MutableList<*>) {
            isCorrectOrder = checkPacketPair(left[i] as MutableList<Any>, right[i] as MutableList<Any>)

            if (isCorrectOrder == true) {
                return true
            } else if (isCorrectOrder == false) {
                return false
            }
        // both list check
        } else {
            if (left[i] is Int) {
                isCorrectOrder = checkPacketPair(mutableListOf(left[i] as Int), right[i] as MutableList<Any>)
            } else if (right[i] is Int) {
                isCorrectOrder = checkPacketPair(left[i] as MutableList<Any>, mutableListOf(right[i] as Int))
            }

            if (isCorrectOrder == true) {
                return true
            } else if (isCorrectOrder == false) {
                return false
            }
        }
        i++
    }
}

fun main() {
    // part 1
    val input1 = File("input.txt")
        .readText()
        .split("\n\n")
        .map { pair -> pair.split("\n").map { it.substring(1) } }

    var sumOfCorrectIndices = 0
    input1.forEachIndexed { index, (leftLine, rightLine) ->
        val left = processInputLine(leftLine).first
        val right = processInputLine(rightLine).first

        val pairInCorrectOrder = checkPacketPair(left, right)
        if (pairInCorrectOrder == true) {
            sumOfCorrectIndices += index + 1
        }
    }
    println("part 1 answer: $sumOfCorrectIndices")

    // part 2
    val input2 = File("input.txt")
        .readText()
        .split("\n")
        .filter { it.isNotEmpty() }
        .map { it.substring(1) }
        .toMutableList()
    input2.add("[2]]")
    input2.add("[6]]")

    val packets = mutableListOf<MutableList<Any>>()
    input2.forEach { line ->
        packets.add(processInputLine(line).first)
    }

    val orderedPackets = mutableListOf<MutableList<Any>>()
    while (packets.size != 0) {
        for (i in packets.indices) {
            var isMostCorrect = true
            for (j in i + 1 .. packets.lastIndex) {
                if (checkPacketPair(packets[i], packets[j]) == false) {
                    isMostCorrect = false
                    break
                }
            }
            if (isMostCorrect) {
                orderedPackets.add(packets[i])
                packets.removeAt(i)
                break
            }
        }
    }

    var decoderKey = 1
    orderedPackets.forEachIndexed { index, packet ->
        if (packet.toString() == "[[2]]" || packet.toString() == "[[6]]") {
            decoderKey *= (index + 1)
        }
    }
    println("part 2 answer: $decoderKey")
}